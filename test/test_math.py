import examples
import operator

#from examples.math import Math

m = examples.Math()

def test_sum():  
    """Check is sum method is equivalent to operator"""
    eq_(m.sum(1, 1), operator.add(1, 1))


def test_sub():  
    """Check is sub method is equivalent to operator"""
    eq_(m.sub(2, 1), operator.sub(2, 1))


def test_mul():  
    """Check is mul method is equivalent to operator"""
    eq_(m.mul(1, 1), operator.mul(1, 1))


if __name__ == '__main__':
    unittest.main()

# The end...
